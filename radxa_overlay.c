#include <errno.h>
#include <fcntl.h>
#include <log/log.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define LOG_TAG "radxa_overlay"

#define DEBUG_LOG 1   //open debug info

#define VENDOR_REQ_TAG		0x56524551
#define VENDOR_READ_IO		_IOW('v', 0x01, unsigned int)
#define VENDOR_WRITE_IO		_IOW('v', 0x02, unsigned int)

#define VENDOR_RADXA_DISPLAY_ID		32

#define OVERLAY_BUF_LEN 128

typedef		unsigned short	  uint16;
typedef		unsigned long	    uint32;
typedef		unsigned char	    uint8;

struct rk_vendor_req {
    uint32 tag;
    uint16 id;
    uint16 len;
    uint8 data[OVERLAY_BUF_LEN];
};

static char overlay_data[OVERLAY_BUF_LEN] = {0};

int overlay_read(uint32 id)
{
    if (DEBUG_LOG) 
			SLOGD("read overlay_data: \n");
    int ret ;
    uint16 len;
    struct rk_vendor_req req;
    memset(overlay_data,0,sizeof(overlay_data));
    int sys_fd = open("/dev/vendor_storage",O_RDONLY,0);
    if(sys_fd < 0){
        SLOGE("vendor_storage open fail %s\n", strerror(errno));
        return -1;
    }

    req.tag = VENDOR_REQ_TAG;
    req.id = id;
    req.len = OVERLAY_BUF_LEN; /* max read length to read*/
    ret = ioctl(sys_fd, VENDOR_READ_IO, &req);
    close(sys_fd);

    if(ret){
        SLOGE("overlay read error\n");
        return -1;
    }

    len = req.len;
    if(len <= 0)
    {
        SLOGE("overlay read error, len <= 0\n");
    }

    memcpy(overlay_data,req.data,len);
    if (DEBUG_LOG)
		SLOGD("overlay read data :%s\n",overlay_data);

	return 0;
}

int overlay_write(uint32 id,const char* overlay_data)
{
    if (DEBUG_LOG) 
			SLOGD("save overlay_data: %s \n",overlay_data);
    int ret ;
    struct rk_vendor_req req;
    int sys_fd = open("/dev/vendor_storage",O_RDONLY,0);
    if(sys_fd < 0){
        SLOGE("vendor_storage open fail %s\n", strerror(errno));
        return -1;
    }
    memset(&req, 0, sizeof(req));
    req.tag = VENDOR_REQ_TAG;
    req.id = id;
    req.len = strlen(overlay_data);
    memcpy(req.data, overlay_data, strlen(overlay_data));

    ret = ioctl(sys_fd, VENDOR_WRITE_IO, &req);
    close(sys_fd);
    if(ret){
        SLOGE("error write overlay data.\n");
        return -1;
    }
    return 0;
}

int main( int argc, char *argv[] )
{
    SLOGD("----------------running overlay---------------");
	uint32 id = atoi(argv[1]);
	const char* overlay_data = argv[2];
    if (argc > 2) {
        overlay_write(id,overlay_data);
    } else {
		overlay_read(id);
	}
    return 0;
}
